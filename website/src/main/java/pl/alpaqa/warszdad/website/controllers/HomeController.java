package pl.alpaqa.warszdad.website.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.alpaqa.warszdad.common.domain.entity.Product;
import pl.alpaqa.warszdad.common.services.ProductService;
import pl.alpaqa.warszdad.common.services.TestService;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/")
@Slf4j
public class HomeController {
    private final TestService testService;
    private final ProductService productService;

    @Autowired
    public HomeController(TestService testService, ProductService productService) {
        this.testService = testService;
        this.productService = productService;
    }

    @RequestMapping("/")
    public ModelAndView home() {
        return new ModelAndView("index");
    }

    @RequestMapping("/hello")
    public String hello() {
        Product product = new Product();
        product.setName("Produkt z dnia: " + LocalDateTime.now().toString());
        productService.addProduct(product);

        for (Product p : productService.getAllProducts()) {
            log.info("Product on the list: " + p.getId() + " - " + p.getName());
        }
        return testService.getText();
    }
}
