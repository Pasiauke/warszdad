package pl.alpaqa.warszdad.website;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = { "pl.alpaqa.warszdad.website", "pl.alpaqa.warszdad.common" })
@EnableMongoRepositories(basePackages = "pl.alpaqa.warszdad.common.repositories")
public class WarszDadWebsiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarszDadWebsiteApplication.class, args);
	}

}

