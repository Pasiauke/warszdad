package pl.alpaqa.warszdad.backoffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = { "pl.alpaqa.warszdad.backoffice", "pl.alpaqa.warszdad.common" })
@EnableMongoRepositories(basePackages = "pl.alpaqa.warszdad.common.repositories")
public class WarszDadBackofficeApplication {
	public static void main(String[] args) {
		SpringApplication.run(WarszDadBackofficeApplication.class, args);
	}
}

