package pl.alpaqa.warszdad.backoffice.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.alpaqa.warszdad.common.domain.entity.ProductMaterial;
import pl.alpaqa.warszdad.common.domain.entity.ProductType;
import pl.alpaqa.warszdad.common.domain.entity.User;
import pl.alpaqa.warszdad.common.domain.model.MaterialType;
import pl.alpaqa.warszdad.common.repositories.ProductMaterialRepository;
import pl.alpaqa.warszdad.common.repositories.ProductTypeRepository;
import pl.alpaqa.warszdad.common.repositories.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class InitialService {

    private final UserRepository userRepository;

    private final ProductTypeRepository productTypeRepository;

    private final ProductMaterialRepository productMaterialRepository;

    @Autowired
    public InitialService(UserRepository userRepository, ProductTypeRepository productTypeRepository, ProductMaterialRepository productMaterialRepository) {
        this.userRepository = userRepository;
        this.productTypeRepository = productTypeRepository;
        this.productMaterialRepository = productMaterialRepository;
    }

    @PostConstruct
    public void init() {
        if (userRepository.findAdminUserByLogin("admin") == null) {
            log.info("No admin user... Initialize!");
            User user = new User();
            user.setLogin("admin");
            user.setFirstName("Pawel");
            user.setLastName("Kolczyk");
            user.setEmail("dilichon@gmail.com");
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            user.setPassword(bCryptPasswordEncoder.encode("password"));
            userRepository.save(user);
        }

        if (CollectionUtils.isEmpty(productTypeRepository.findAll())) {
            List<ProductType> productTypes = new ArrayList<>();
            productTypes.add(new ProductType("Deska do krojenia"));
            productTypes.add(new ProductType("Deska do podawania"));
            productTypes.add(new ProductType("Meble"));
            productTypes.add(new ProductType("Inne"));

            productTypeRepository.saveAll(productTypes);
        }

        if (CollectionUtils.isEmpty(productMaterialRepository.findAll())) {
            List<ProductMaterial> productMaterials = new ArrayList<>();

            productMaterials.add(new ProductMaterial(MaterialType.WOOD, "Dąb"));
            productMaterials.add(new ProductMaterial(MaterialType.WOOD, "Czerwony Dąb"));
            productMaterials.add(new ProductMaterial(MaterialType.WOOD, "Jesion"));
            productMaterials.add(new ProductMaterial(MaterialType.WOOD, "Akacja"));
            productMaterials.add(new ProductMaterial(MaterialType.WOOD, "Lipa"));
            productMaterials.add(new ProductMaterial(MaterialType.WOOD, "Sosna"));
            productMaterials.add(new ProductMaterial(MaterialType.WOOD, "Brzoza"));

            productMaterials.add(new ProductMaterial(MaterialType.METAL, "Stal"));
            productMaterials.add(new ProductMaterial(MaterialType.METAL, "Żelazo"));
            productMaterials.add(new ProductMaterial(MaterialType.METAL, "Miedź"));
            productMaterials.add(new ProductMaterial(MaterialType.METAL, "Złoto"));
            productMaterials.add(new ProductMaterial(MaterialType.METAL, "Srebro"));

            productMaterials.add(new ProductMaterial(MaterialType.STONE, "Granit"));
            productMaterials.add(new ProductMaterial(MaterialType.STONE, "Kwarc"));
            productMaterials.add(new ProductMaterial(MaterialType.STONE, "Ametyst"));

            productMaterialRepository.saveAll(productMaterials);

        }
    }
}
