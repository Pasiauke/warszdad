package pl.alpaqa.warszdad.backoffice.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.alpaqa.warszdad.common.domain.entity.User;
import pl.alpaqa.warszdad.common.repositories.UserRepository;

@Service
public class UserDetailsSecurityService implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public UserDetailsSecurityService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findAdminUserByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        } else {
            return new AdminUserDetails(user);
        }
    }
}
