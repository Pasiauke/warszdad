package pl.alpaqa.warszdad.common.domain.model;

public enum ProductStatus {
    ACTIVE,
    INACTIVE,
    DELETED
}
