package pl.alpaqa.warszdad.common.domain.model;

public enum OrderStatus {
    NEW,
    INACTIVE,
    ORDERED,
    IN_PROGRESS,
    CLOSED,
    REJECTED
}
