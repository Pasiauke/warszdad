package pl.alpaqa.warszdad.common.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.alpaqa.warszdad.common.domain.entity.Product;

@Repository
public interface ProductRepository extends MongoRepository<Product, ObjectId> {
}
