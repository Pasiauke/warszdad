package pl.alpaqa.warszdad.common.domain.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.alpaqa.warszdad.common.domain.model.MaterialType;

@Getter
@Setter
@ToString
@Document("product_materials")
public class ProductMaterial {

    public ProductMaterial(){}

    public ProductMaterial(MaterialType materialType, String name) {
        this.setType(materialType);
        this.setName(name);
    }

    @Id
    private String id;

    private String name;

    private MaterialType type;
}
