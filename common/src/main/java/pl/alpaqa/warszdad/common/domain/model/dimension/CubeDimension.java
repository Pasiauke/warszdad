package pl.alpaqa.warszdad.common.domain.model.dimension;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class CubeDimension extends BaseDimension {

    private int length;
    private int height;
    private int width;

    public CubeDimension(int length, int height, int width, String description) {
        this.length = length;
        this.height = height;
        this.width = width;
        this.description = description;
    }

    @Override
    String getDimensions() {
        return null;
    }
}
