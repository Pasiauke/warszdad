package pl.alpaqa.warszdad.common.domain.model.dimension;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class PyraminDimension extends BaseDimension {

    private int height;
    private int baseLength;
    private int baseWidth;

    public PyraminDimension(int height, int baseLength, int baseWidth, String description) {
        this.height = height;
        this.baseLength = baseLength;
        this.baseWidth = baseWidth;
        this.description = description;
    }

    @Override
    String getDimensions() {
        return null;
    }
}
