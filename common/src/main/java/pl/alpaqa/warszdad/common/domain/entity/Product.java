package pl.alpaqa.warszdad.common.domain.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.alpaqa.warszdad.common.domain.model.ProductStatus;
import pl.alpaqa.warszdad.common.domain.model.dimension.Dimensions;
import pl.alpaqa.warszdad.common.domain.model.MaterialType;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@Document("products")
public class Product {
    @Id
    private String id;

    private String typeId;

    private MaterialType material;

    private List<String> materialList;

    private String name;

    private String description;

    private String serialNumber;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private ProductStatus status;

    private Double minPrice;

    private Double maxPrice;

    private Dimensions dimensions;

    private Integer mass;

    private String mainImageName;

    private List<String> imagesNames;

}
