package pl.alpaqa.warszdad.common.domain.model.dimension;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class CylinderDimension extends BaseDimension {

    private int height;
    private int radius;

    public CylinderDimension(int height, int radius, String description) {
        this.height = height;
        this.radius = radius;
        this.description = description;
    }

    @Override
    String getDimensions() {
        return null;
    }
}
