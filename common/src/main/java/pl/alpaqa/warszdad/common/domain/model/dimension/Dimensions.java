package pl.alpaqa.warszdad.common.domain.model.dimension;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Dimensions extends BaseDimension {

    private int length;
    private int height;
    private int width;

    private List<BaseDimension> other = new ArrayList<>();

    @Override
    String getDimensions() {
        return null;
    }
}
