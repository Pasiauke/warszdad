package pl.alpaqa.warszdad.common.services;

import pl.alpaqa.warszdad.common.domain.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();
    void addProduct(Product product);
}
