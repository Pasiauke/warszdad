package pl.alpaqa.warszdad.common.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.alpaqa.warszdad.common.domain.entity.ProductType;

@Repository
public interface ProductTypeRepository extends MongoRepository<ProductType, ObjectId> {
}
