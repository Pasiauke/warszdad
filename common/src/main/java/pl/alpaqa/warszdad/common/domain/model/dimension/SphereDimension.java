package pl.alpaqa.warszdad.common.domain.model.dimension;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public final class SphereDimension extends BaseDimension {

    private int radius;

    public SphereDimension(int radius, String description) {
        this.radius = radius;
        this.description = description;
    }

    @Override
    String getDimensions() {
        return null;
    }
}
