package pl.alpaqa.warszdad.common.domain.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@Document("product_types")
public class ProductType {

    public ProductType(){}
    public ProductType(String name) {
        this.setName(name);
    }

    @Id
    private String id;

    private String name;
}
