package pl.alpaqa.warszdad.common.domain.model.dimension;

abstract class BaseDimension {
    String description;
    abstract String getDimensions();
}
